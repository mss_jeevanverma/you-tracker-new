<?php

$app->config(array(
   'templates.path' => './view'
));
$app->get('/', function (){
	echo "sdfsdf";
});
$app->post('/phoneverify',"phoneNumberVerify");
$app->post('/register',"registerNewUser");
$app->post('/registerimage',"registerNewUserImage");
$app->post('/getregisterids',"getRegisteredUsers");
$app->post('/sendrequests',"sendRequestsToJoin");
$app->post('/sendconfirmation',"sendConfirmationToJoin");
$app->post('/sendrejection',"sendRejectionToJoin");
$app->get('/getcategory',"getCategoryToJoin");
$app->post('/updateuser','updateUserInfo');
$app->post('/cancelmeet','cancelMeeting');
$app->post('/leavemeet','leaveMeeting');
$app->post('/visibility','userVisibility');
$app->post('/locationupdate','locationUpdate');
$app->post('/alleventusers','allEventUsers');
$app->post('/edituserinfo','editUserInfo');
$app->post('/update_event','updateEvent');
$app->get('/about-us', function () use ($app) {
    $app->render('about-us.php');
});
?>